package view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;

public class BankAccountFrame extends JFrame {
	 private static final int FRAME_WIDTH = 450;
	   private static final int FRAME_HEIGHT = 100;

	  // private static final double DEFAULT_RATE = 5;
	  // private static final double INITIAL_BALANCE = 1000;   

	   private JLabel rateLabel;
	   private JTextField rateField;
	   private JButton button;
	   private JLabel resultLabel;
	   private JPanel panel;
	   //private BankAccount account;
	   
	   
	   public BankAccountFrame(BankAccount b) { 
	     // account = new BankAccount(INITIAL_BALANCE);
	      resultLabel = new JLabel("balance: " + b.getBalance());
	      //createTextField();
	      //createButton();
	      //createPanel();
	      
	      //setVisible(true);
	     // setSize(FRAME_WIDTH, FRAME_HEIGHT);
	   }
	   public void createTextField(double x)
	   {
	      rateLabel = new JLabel("Interest Rate: ");

	      final int FIELD_WIDTH = 10;
	      rateField = new JTextField(FIELD_WIDTH);
	      rateField.setText("" + x);
	   }
	   
	   public void createButton(BankAccount ac)
	   {
	      button = new JButton("Add Interest");
	      
	      class AddInterestListener implements ActionListener
	      {
	         public void actionPerformed(ActionEvent event)
	         {
	            double rate = Double.parseDouble(rateField.getText());
	            double interest = ac.getBalance() * rate / 100;
	            ac.deposit(interest);
	            resultLabel.setText("balance: " + ac.getBalance());
	         }            
	      }
	      
	      ActionListener listener = new AddInterestListener();
	      button.addActionListener(listener);
	   }

	   public void createPanel()
	   {
	      panel = new JPanel();
	      panel.add(rateLabel);
	      panel.add(rateField);
	      panel.add(button);
	      panel.add(resultLabel);      
	      add(panel);
	   } 
	   

}
