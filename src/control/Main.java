package control;
import view.BankAccountFrame;
import model.BankAccount;
 
public class Main {
	
	public BankAccountFrame View;
	private BankAccount account;
	private static final double INITIAL_BALANCE = 1000;
	private static final double DEFAULT_RATE = 5;
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;

	public static void main(String[] args) {
		new Main();

	}
	public Main(){
		account = new BankAccount(INITIAL_BALANCE);
		View = new BankAccountFrame(account);
		View.createTextField(DEFAULT_RATE);
		View.createButton(account);
		View.createPanel();
		View.setVisible(true);
	    View.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}

}
